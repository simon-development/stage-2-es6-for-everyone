import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  firstFighter.healthLeft = firstFighter.health;
  secondFighter.healthLeft = secondFighter.health;
  console.log(firstFighter);
  console.log(secondFighter);
  let pressed = new Set();
  let firstFighterLastCriticalUsageDate = Date.now();
  let secondFighterLastCriticalUsageDate = Date.now();

  function checkCritical() {
    let firstPressedCritical = true;
    let secondPressedCritical = true;
    for (let code of controls.PlayerOneCriticalHitCombination) {
      if (!pressed.has(code)) {
        console.log('not has ' + code);
        firstPressedCritical = false;
      } else {console.log('yes has ' + code);}
    }
    for (let code of controls.PlayerTwoCriticalHitCombination) {
      if (!pressed.has(code)) {
        secondPressedCritical = false;
      }
    }
    console.log(Date.now() - firstFighterLastCriticalUsageDate);
    if (firstPressedCritical && (Date.now() - firstFighterLastCriticalUsageDate >= 10000)) {
      const demage = getCriticalHitPower(firstFighter);
      secondFighter.healthLeft = Math.max(0, secondFighter.healthLeft - demage);
      firstFighterLastCriticalUsageDate = Date.now();
      console.log('first critical kick');
    }
    if (secondPressedCritical && (Date.now()- secondFighterLastCriticalUsageDate >= 10000)) {
      const demage = getCriticalHitPower(secondFighter);
      firstFighter.healthLeft = Math.max(0, firstFighter.healthLeft - demage);
      secondFighterLastCriticalUsageDate = Date.now();
      console.log('second critical kick');
    }
    updateHealth(firstFighter, secondFighter);
    console.log('check kick');
  }
  
  const weHaveAWinnerPromise = new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', function(event) {
      pressed.add(event.code);
      console.log('key down ' + event.code);
      checkCritical();
    });
  
    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
      let code = event.code;
      if (code === controls.PlayerOneAttack && !pressed.has(controls.PlayerOneBlock)) {
        const demage = pressed.has(controls.PlayerTwoBlock) ? getDamage(firstFighter, secondFighter) : getHitPower(firstFighter);
        secondFighter.healthLeft = Math.max(0, secondFighter.healthLeft - demage);
        updateHealth(firstFighter, secondFighter);
      }
      if (code === controls.PlayerTwoAttack && !pressed.has(controls.PlayerTwoBlock)) {
        const demage = pressed.has(controls.PlayerOneBlock) ? getDamage(secondFighter, firstFighter) : getHitPower(secondFighter);
        firstFighter.healthLeft = Math.max(0, firstFighter.healthLeft - demage);
        updateHealth(firstFighter, secondFighter);
      }
      console.log(firstFighter);
      console.log(secondFighter);
      if (firstFighter.healthLeft === 0 || secondFighter.healthLeft === 0) {
        let winner = firstFighter.healthLeft === 0 ? secondFighter : firstFighter;
        resolve(winner);
      }    
    });
  });

  return weHaveAWinnerPromise;
}

export function getDamage(attacker, defender) {
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
  // return damage
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
  // return hit power
}

export function getCriticalHitPower(fighter) {
  return fighter.attack * 2;
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1)
  // return block power
}

function updateHealth(firstFighter, secondFighter) {
  const firstFighterIndicator = document.getElementById('left-fighter-indicator');
  const secondFighterIndicator = document.getElementById('right-fighter-indicator');
  firstFighterIndicator.style.width = firstFighter.healthLeft / firstFighter.health * 100 + '%';
  secondFighterIndicator.style.width = secondFighter.healthLeft / secondFighter.health * 100 + '%';
}